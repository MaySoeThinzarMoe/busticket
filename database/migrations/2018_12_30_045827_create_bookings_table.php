<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBookingsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bookings', function (Blueprint $table) {
            $table->increments('bookid');
            $table->integer('userbookid')->unsigned()->on('users');
            $table->integer('companybookid')->unsigned()->on('companies');
            $table->string('passengername');
            $table->string('passengerphone');
            $table->string('passengernrc');
            $table->string('pickuplocation');
            $table->string('dropoflocation');
            $table->integer('seatno');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bookings');
    }
}
