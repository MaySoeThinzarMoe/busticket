<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoutesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    /*$table->increments('id');
            $table->string('title');
            $table->string('photo');
            $table->text('body');
            $table->integer('user_id')->unsigned()->on('users');
            $table->foreign('user_id')->references('id')->on('users');
            $table->integer('category_id')->unsigned()->on('categories');
            $table->foreign('category_id')->references('id')->on('categories');
            $table->timestamps();*/
    public function up()
    {
        Schema::create('routes', function (Blueprint $table) {
            $table->increments('rid');
            $table->string('rfrom');
            $table->string('rto');
            $table->integer('companyrid')->unsigned()->on('companies');
            $table->integer('busrid')->unsigned()->on('buses');
            $table->string('departuretime');
            $table->string('arrivaltime');
            $table->string('durationestimate');
            
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('routes');
    }
}
