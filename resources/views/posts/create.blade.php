@extends('layouts.template')

@section('content')
	<div class="col-md-8 mt-5">
		@if(count($errors))
		<div class="alert alert-danger">
			<ul>
				@foreach($errors->all() as $error)
					<li>{{ $error}}</li>>
				@endforeach
			</ul>
		</div>	
		@endif
		<form action="/upload" method="post" enctype="multipart/form-data">
			@csrf
			<div class="form-group">
				<label>Title:</label>
				<input type="text" name="title" class="form-control">
			</div>
			<div class="form-group">
				<label>Photo:</label>
				<input type="file" name="photo" class="form-control-file">
			</div>
			<div class="form-group">
				<label>Choose Category:</label>
				<select name="categorylist">
					@foreach($categories as $category)
						<option class="form-control" value="{{ $category->id }}">{{ $category->category_name }}</option>
					@endforeach
				</select>
			</div>
			<div class="form-group">
				<label>Body:</label>
				<textarea name="body" class="form-control" id="summernote"></textarea>
			</div>
			<div class="form-group">
				<input type="submit" name="submit" class="btn btn-primary">
			</div>
		</form>
	</div>
@endsection