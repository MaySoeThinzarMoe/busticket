<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PostResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return[
            'pid'=> $this->id,
            'title'=>$this->title,
            'body'=>$this->body,
            'written at'=>$this->created_at->diffForHumans(),
        ];
        //return parent::toArray($request);
    }
}
